<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>RevisePage</title>
</head>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#revise").click(function(){
			var check_num = 2147483648;
			var price = $("#price").val();
			
			if($("#menu").val() == ""){
				alert("메뉴명을 입력해 주세요.");
				$("#menu").focus();
				return false;
			}

			if($("#price").val() == ""){
				alert("가격 입력해 주세요.");
				$("#price").focus();
				return false;
			}
			if(Number(price) >= check_num){
				alert("입력값이 너무 큽니다");
				$("#price").focus();
				return false;
			}

			if($("#origin").val() == ""){
				$("#origin").focus();
				alert("원산지을 입력해 주세요.");
				return false;
			}
		});
	});
</script>


<body>
	<form action="inrevise" method="post">
		<input type="hidden" id="id" name="Id" value = "${menu.id}" ><br>
		Menu :<br>
		<input type="text" id="menu" name = "Menu" value = "${menu.menu}"> <br> 
		Price :<br>
		<input type="text" id="price" name = "Price" value = "${menu.price}"> <br>
		Origin : <br>
		<input type="text" id="origin" name = "Origin" value = "${menu.origin}" ><br>

		<button id="revise">수정</button>
	</form>
</body>
</html>