<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>MainPage</title>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
	$(document).ready(function(){
		$('tr').click(function(){
			var id = $(this).find('#idNo').text()
			location.href = "http://localhost:8080/revise?id=" + id;
		});
	});
</script>
</head>

<body>
<table border = "2" style = "width: 100%">
    <thead>
    <tr>
        <td align="center">메뉴</td>
        <td align="center">가격</td>
        <td align="center">원산지</td>
    </tr>
    </thead>
<c:forEach var="item" items="${menu}" varStatus="status">
	<tr style="cursor:pointer;">
		<td id="idNo" style="display:none;">${item.id}</td> 
		<td align="center"> ${item.menu} </td>
		<td align="center"> ${item.price } </td>
		<td align="center"> ${item.origin } </td> 
    </tr>
</c:forEach>
</table>
<button type="button" onclick="location.href='http://localhost:8080/addmenu'">메뉴등록</button>

</body>
</html>


	