<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Insert Menu</title>
</head>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#add").click(function(){
			var check_num = 2147483648;
			var price = $("#price").val();
			
			if($("#menu").val() == ""){
				alert("메뉴명을 입력해 주세요.");
				$("#menu").focus();
				return false;
			}
			
			if($("#price").val() == ""){
				alert("메뉴명을 입력해 주세요.");
				$("#price").focus();
				return false;
			}

			if(Number(price) >= check_num){
				alert("입력값이 너무 큽니다");
				$("#price").focus();
				return false;
			}
			
			if($("#origin").val() == ""){
				alert("메뉴명을 입력해 주세요.");
				$("#origin").focus();
				return false;
			}
		});
});

</script>

<body>
	<form action="addmenupage" method="post">     
		Menu :<br>
		<input type="text" id="menu" name="Menu"><br> 
		Price :<br>
		<input type="text" id="price" name="Price"><br>
		Origin : <br>
		<input type="text" id="origin" name="Origin"><br>
		<button id="add">등록</button>
	</form>
</body>
</html>