package com.example.demo.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.example.demo.model.MenuModel;

@Mapper
public interface MenuMapper {
    List<MenuModel> getAllProducts();
    public String getalldata();
    void putMenu(MenuModel vos);
    void reviseMenu(MenuModel vos);
	MenuModel selectProduct(int id);
}