package com.example.demo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.MenuModel;
import com.example.demo.service.MenuServiceImpl;

@Controller 
public class BoardController { 
//    @Resource(name="MenuService")
	@Autowired
	private MenuServiceImpl menuService;
	    
    @Autowired
    DataSource dataSource;
    
	@RequestMapping("/")
	public Object getMenu(@ModelAttribute("vo") MenuModel vo) throws SQLException {	
		System.out.println("BoardController");
		ModelAndView modelAndView = new ModelAndView();
		List<MenuModel> productList = menuService.selectAllProducts();
		modelAndView.setViewName("/MainBoard");
    	
	    return modelAndView.addObject("menu", productList);
	}
//	@RequestMapping("/")
//	public String getMenu(Model model) throws SQLException {
//		model.addAttribute("menu", menuService.selectAllProducts());
//        return "MainBoard";
//	}
	
	@RequestMapping("/revise")
	public Object showRevisePage(@RequestParam(value = "id", required=false) int id) { 
		System.out.println("revisecontroller");
		ModelAndView modelAndView = new ModelAndView();		
		
		MenuModel searchMenu = new MenuModel();

		searchMenu =  menuService.getProduct(id);
		modelAndView.setViewName("/RevisePage");
		return modelAndView.addObject("menu", searchMenu);
	}
	

//	@RequestMapping("/revise")
//	public String showRevisePage(@RequestParam(value = "id", required=false) int id, Model model) { 
//		System.out.println("revisecontroller");
//		
//		MenuModel searchMenu = new MenuModel();
//
//		searchMenu =  menuService.getProduct(id);
//		model.addAttribute("menu", searchMenu);
//
//		return "RevisePage"; 
//	}
	
	@RequestMapping("inrevise")
	public Object reviseMenu(@ModelAttribute("menu") MenuModel reviseMenu)  throws SQLException {
		System.out.println("inrevisecontroller");
		
		menuService.updateMenu(reviseMenu);

		return new ModelAndView("redirect:/");
	}
//	@RequestMapping("/inrevise")
//	public String reviseMenu(@RequestParam(value = "reviseid", required=false) int id, @RequestParam(value = "revisemenu", required=false) String menu, @RequestParam(value = "reviseprice", required=false) int price, @RequestParam(value = "reviseorigin", required=false) String origin,  Model model)  throws SQLException {
//		System.out.println("inrevisecontroller");
//		
//		MenuModel reviseMenu = new MenuModel();
//
//		reviseMenu.setId(id);
//		reviseMenu.setMenu(menu);
//		reviseMenu.setPrice(price);
//		reviseMenu.setOrigin(origin);
//		
//		try {
//			menuService.updateMenu(reviseMenu);
//		} 
//		catch (Exception e) {
//			System.out.println("fail error" + e);
//		}
//		
//		model.addAttribute("menu", menuService.selectAllProducts());
//		return "MainBoard"; 
//	}
	
	@RequestMapping("/addmenu")
	public String addMenuPage() { 
		return "AddMenu"; 
	} 
	
	@RequestMapping("/addmenupage") 
	public Object addMenu(@ModelAttribute("menu") MenuModel addMenu) {
		System.out.println("addmenucontroller");

		menuService.insertMenu(addMenu);

		return new ModelAndView("redirect:/");
	}
	
//	@RequestMapping("/addmenupage") 
//	public String addMenu(@RequestParam("addmenu") String menu, @RequestParam("addprice") int price, @RequestParam("addorigin") String origin, Model model) {
//		System.out.println("addmenucontroller");
//		
//		MenuModel addMunu = new MenuModel();
//		addMunu.setMenu(menu);
//		addMunu.setPrice(price);
//		addMunu.setOrigin(origin);
//		
//		menuService.insertMenu(addMunu);
//
//		model.addAttribute("menu", menuService.selectAllProducts());
//		
//		return "MainBoard"; 
//	}
}