package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.repository.*;
import com.example.demo.model.MenuModel;

//@Service("MenuSerivce")
@Service
public class MenuServiceImpl implements MenuService{
    @Autowired
    private MenuMapper menuMapper;

	@Override
	public List<MenuModel> selectAllProducts() {
		// TODO Auto-generated method stub
        return menuMapper.getAllProducts();
	}

	@Override
	public String selectalldata() {
		// TODO Auto-generated method stub
    	return menuMapper.getalldata();
	}
	
    @Transactional("transactionManager")
    public void insertMenu(MenuModel vos) {
    	try {
    		menuMapper.putMenu(vos);
    	}
    	catch (Exception e) {
    		System.out.println("insert Error : " + e);
    	}
    }
    
    @Transactional("transactionManager")
    public void updateMenu(MenuModel vos) {
    	try {
    		menuMapper.reviseMenu(vos);
    	}
    	catch (Exception e2) {
    		System.out.println("update Error : " + e2);
    	}
    }
    
    @Override
    public MenuModel getProduct(int id){
    	return menuMapper.selectProduct(id);
    }
}