package com.example.demo.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import com.example.demo.model.MenuModel;

public interface MenuService {
    List<MenuModel> selectAllProducts();
    public String selectalldata();
    MenuModel getProduct(int id);
    void insertMenu(MenuModel vos);
    void updateMenu(MenuModel vos);
}
