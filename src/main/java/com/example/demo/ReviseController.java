//package com.example.demo;
//
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import com.example.demo.model.MenuModel;
//import com.example.demo.service.MenuServiceImpl;
//
//@Controller 
//public class ReviseController {
//
//	@Autowired	
//	private MenuServiceImpl menuService;
//	    
//    @Autowired
//    DataSource dataSource;
//
//	@RequestMapping("inrevise")
//	public String Revisemenu(@RequestParam(value = "reviseid", required=false) int id, @RequestParam(value = "revisemenu", required=false) String menu, @RequestParam(value = "reviseprice", required=false) int price, @RequestParam(value = "reviseorigin", required=false) String origin,  Model model) {
//		System.out.println("inrevisecontroller");
//		
//		MenuModel reviseMenu = new MenuModel();
//
//		reviseMenu.setId(id);
//		reviseMenu.setMenu(menu);
//		reviseMenu.setPrice(price);
//		reviseMenu.setOrigin(origin);
//		
//		menuService.updateMenu(reviseMenu);
//			
//		model.addAttribute("menu", menuService.selectAllProducts());
//		return "MainBoard"; 
//	}
//}
