package com.example.demo.model;

public class MenuModel {
	private String Menu;
	private int Price;
	private String Origin;
	private int Id;
	
	public int getId() {
		return Id;
	}
	
	public void setId(int Id) {
		this.Id = Id;
	}
	
	public String getMenu() {
		return Menu;
	}
	
	public void setMenu(String Menu) {
		this.Menu = Menu;
	}
	
	public int getPrice() {
		return Price;
	}
	
	public void setPrice(int Price) {
		this.Price = Price;
	}
	
	public String getOrigin() {
		return Origin;
	}
	
	public void setOrigin(String Origin) {
		this.Origin = Origin;
	}
}
